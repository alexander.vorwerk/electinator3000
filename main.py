#!/usr/bin/env python
# -*- coding: utf-8 -*-
from bottle import route, run, template, get,post, request, static_file
import random
import string
import sqlite3
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import smtplib

db = sqlite3.connect('data.db', check_same_thread=False)
cur=db.cursor()
cur.execute("CREATE TABLE IF NOT EXISTS anfragen( name text, vorname text, fak text, frei text, bday text, nummer text, email text, tmpCheck text, stupa boolean, senat boolean, fsr text, fakra text)")
db.commit()

def sendMail(email, tmpCheck):
    s = smtplib.SMTP('mail.YouerServer')
    s.starttls()
    s.login('email@addresse.de', 'your password')
    msg=MIMEMultipart()
    message="Hallo, bitte klick auf den link, um deine Solikandidatur für die GHG zu bestätigen. <br> <a href='http://ghg.uni-goettingen.de/validate/"+tmpCheck+"'>validate</a>"
    msg['From']="noreply@yourServer"
    msg['To']=email
    msg['Subject']='Solikandidatur für die GHG'
    msg.attach(MIMEText(message,'html'))
    s.send_message(msg)
    del msg



@get('/')
@get('//')
def form():
    return open('form.html').read()

@post('/')
@post('//')
def save():
    paras=request.POST.decode()
    name=paras['name'].replace(","," ").replace(";"," ")#request.forms.get("name") #.decode('utf-8')
    vorname=paras['vorname'].replace(","," ").replace(";"," ")#request.forms.get('vorname')
    email=paras['email'].replace(","," ").replace(";"," ")#request.forms.get('email')
    if not(email.endswith("@stud.uni-goettingen.de")):
        return "das ist keine Uni addresse <a href='/'> nochmal versuchen</a>"
    bday=paras['geburtsdatum'].replace(","," ").replace(";"," ")#request.forms.get('geburtsdatum')
    fak=paras['fakultaet'].replace(","," ").replace(";"," ")#request.forms.get('fakultaet')
    frei=paras['freiw'].replace(","," ").replace(";"," ")#request.forms.get('freiw')
    nummer=paras['matNa'].replace(","," ").replace(";"," ")#request.forms.get('matNa')
    print(paras.keys())
    stupa=False
    senat=False
    if( "stupa" in paras.keys()):
        stupa = True
    if( "senat" in paras.keys()):
        senat = True
    fsr = paras["FSR"]
    fakra=paras["FakRa"]

    tmpCheck=''.join(random.choice(string.ascii_lowercase) for i in range(50))
    cur.execute("INSERT INTO anfragen VALUES('"+name+"','"+vorname+"','"+fak+"','"+frei+"','"+bday+"','"+nummer+"','"+email+"','"+tmpCheck+"',"+str(stupa)+","+str(senat)+",'"+fsr+"','"+fakra+"')")
    db.commit()
    sendMail(email,tmpCheck)
    return "bitte check deine E-Mails"

@get('/validate/<check>')
def validate(check):
    anfr = cur.execute("SELECT name, vorname, fak, frei, bday, nummer, email FROM anfragen WHERE tmpCheck='"+check+"'").fetchone()
    places = cur.execute("SELECT stupa, senat, fsr, fakra  FROM anfragen WHERE tmpCheck='"+check+"'").fetchone()
    if anfr==None:
        return 'validation Faild'
    erg=""
    for a in anfr:
        erg+=a+", "
    print(str(places[0]))
    if(places[0]==1):
            with open('stupa.csv','a',encoding='UTF8') as file:
                file.write(erg+"\n")
    if(places[1]==1):
            with open('senat.csv','a',encoding='UTF8') as file:
                file.write(erg+"\n")
    if(places[2]!="keiner"):
            with open(places[2]+".csv",'a',encoding='UTF8') as file:
                file.write(erg+"\n")
    if(places[3]!="keiner"):
            with open(places[3]+"_fakra.csv",'a',encoding='UTF8') as file:
                file.write(erg+"\n")
    cur.execute("DELETE FROM anfragen WHERE tmpCheck='"+check+"'")
    db.commit()

    return "Geschafft!"

run(host='localhost',port=8080)
